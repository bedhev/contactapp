# Contact App

 Project made using a custom MVVM pattern through delegate.
 The deployement target of the project is iOS 14.4 and the project use `cocopoads` (see https://guides.cocoapods.org) as dependency manager in order to use the custom `SwiftLint` script,  `Moya` for the endpoints and SQLITE.swift for the SQLite database.

# Run the project

 - Extract the project then execute `ContactApp.xcworkspace`
 - Select the Target `ContactApp` and use any simulator. (iPhone 8 and superior is prefered).
 
 # Misc
- The project use a custom `Logger` class, if you need to print anything use `Logger.log(...)` instead of print because the SwiftLint script doesn't allow the usage of `print(...)`
- The assignement wasn't about UI so the UI of the application is minimalist and may not work on device below iPhone 8.

# TODO
- Edit user
- Set contact as favorite
- Insert the "complete name" of a user in the local DB for future usage

# Improvements
- Cache the contact's image instead of recreate them everytime the Tableview is scrolled.
- Use the `_id_` from the API to differentiate which contact fetched must be added or removed from the database. Currently the get contact request is only made once then the local data are always used.
- Pull to refresh
