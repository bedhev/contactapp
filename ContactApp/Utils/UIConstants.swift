//
//  UIConstants.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 03/06/2021.
//

import Foundation
import CoreGraphics

enum Cells: String {
    case contact = "ContactCell"
}

enum Xibs: String {
    case contactCell = "ContactCell"
}

enum Rows: CGFloat {
    case contact = 70.0
}

enum Images: String {
    case defaultContact
    case favoriteContact
}
