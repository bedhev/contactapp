//
//  Constants.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation

struct Database {

    static let name = "Contact"
    static let ext = "db"
    static let completeName = name+"."+ext

    enum Contacts: String {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case phone
        case email
        case isFavorite
        case imageName
        case table = "contacts"
    }

    private init() {}
}

struct Base {
    static let URL = "http://34.222.104.175"
    private init() {}
}
