//
//  Endpoint.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 03/06/2021.
//

import Foundation
import Moya

protocol Endpoint: TargetType {
}

extension Endpoint {
    var headers: [String: String]? {
        return nil
    }

    var sampleData: Data {
        return Data()
    }
}
