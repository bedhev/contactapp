//
//  DBHelper.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation

protocol DBHelper {
    associatedtype T
    static func insert(item: T) throws -> Int64
    static func delete(item: T) throws
    static func update(oldItem: T, newItem: T) throws
    static func findAll() throws -> [T]
}
