//
//  ModelWrapper.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation

protocol ModelWrapper {
    var code: Int {get set}
    var message: String {get set}
}
