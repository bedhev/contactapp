//
//  PhoneContact.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation

protocol PhoneContact {
    var firstName: String {get set}
    var lastName: String {get set}
    var email: String {get set}
    var phone: String {get set}
}
