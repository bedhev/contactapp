//
//  BaseNetwork.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 03/06/2021.
//

import Foundation
import Moya

protocol BaseNetwork {
    func processResponse<T: Decodable>(result: Result<Moya.Response, MoyaError>,
                                       decodable: T.Type,
                                       completion: @escaping (T?, Error?) -> Void)
}

extension BaseNetwork {

    func processResponse<T: Decodable>(result: Result<Moya.Response, MoyaError>,
                                       decodable: T.Type,
                                       completion: @escaping (T?, Error?) -> Void) {
        switch result {
        case .success(let response):
            switch self.validateStatusCode(response.statusCode, path: response.request?.url?.path) {
            case .success:
                let data = try? response.map(decodable)
                completion(data, nil)
            case .failure(let error):
                completion(nil, error)
            }
        case .failure(let error):
            completion(nil, error)
        }
    }

    private func validateStatusCode(_ response: Int, path: String?) -> Result<String, APIError> {
        switch response {
        case 200, 201:
            return .success("")
        case 401:
            return .failure(APIError.unauthorized(path: path))
        case 403:
            return .failure(APIError.forbidden(path: path))
        case 400, 404...499:
            return .failure(APIError.notFound(path: path))
        case 500:
            return .failure(APIError.serverIssue)
        default:
            return .failure(APIError.unknownIssue)
        }
    }
}
