//
//  Contact.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation

struct Contact: PhoneContact {
    var id: Int = 0
    var firstName: String
    var lastName: String
    var email: String
    var phone: String
    var isFavorite: Bool = false
    var imageName: String?
    var imageURL: URL? {
        guard let name = imageName else {
            return nil
        }
        return FileSystem.documentsDirectory?.appendingPathComponent(name)
    }
    //TODO: insert full name in DB
    var fullName: String {
        return firstName + " " + lastName
    }

    func isAllInformationSet() -> Bool {
        return !firstName.isEmpty && !lastName.isEmpty && !email.isEmpty && !phone.isEmpty
    }
}

extension Contact: Codable {

    private enum Keys: String, CodingKey {
        case firstName = "firstname"
        case lastName = "lastname"
        case email
        case phone
        case isFavorite
        case imageName
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        firstName = try container.decode(String.self, forKey: .firstName)
        lastName = try container.decode(String.self, forKey: .lastName)
        email = try container.decode(String.self, forKey: .email)
        phone = try container.decode(String.self, forKey: .phone)
    }
}
