//
//  ContactWrapper.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation

struct ContactWrapper: ModelWrapper {
    var code: Int
    var message: String
    var contacts: [Contact]?
    var contact: Contact?
}

extension ContactWrapper: Codable {
}
