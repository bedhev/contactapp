//
//  ContactDetailVM.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 08/06/2021.
//

import UIKit

protocol ContactDetailVMDelegate: class {
    func enableCreation(_ enabled: Bool)
    func didFailWith(error: Error)
    func didSaveContact()
}

enum DetailStatus {
    case create
    case edit
}

enum Field: String {
    case lastName
    case firstName
    case email
    case phone
}

final class ContactDetailVM {

    private weak var delegate: ContactDetailVMDelegate?
    private var contact: Contact?
    private var newContact: Contact!
    private var status: DetailStatus!
    private var newContactImage: Data?

    init(delegate: ContactDetailVMDelegate, contact: Contact? = nil) {
        self.delegate = delegate
        self.contact = contact
        self.status = contact != nil ? .edit : .create
        self.newContact = contact != nil ? contact : Contact(firstName: "", lastName: "", email: "", phone: "")
    }

    func didGetImage(image: UIImage) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.newContactImage = image.pngData()
        }
    }

    func didPressSaveContact() {
        guard newContact.isAllInformationSet() else {
            #warning("This case shouldn't never happen")
            return
        }
        switch status {
        case .create:
            ContactNetworkHelper.shared.createContact(contact: newContact) { [weak self]  (contact, error) in
                DispatchQueue.main.async {
                    guard let _ = contact else {
                        self?.delegate?.didFailWith(error: error!)
                        return
                    }
                    self?.insertNewContact()
                }
            }
        default:
            //TODO: edit profile
            break
        }
    }

    private func insertNewContact() {
        do {
            if newContactImage != nil {
                let timestamp = "\(Date().timeIntervalSince1970)"
                guard let url = FileSystem.documentsDirectory?.appendingPathComponent(timestamp+".png") else {
                    Logger.error(FilesError.documentDirectoryUnavailable.message)
                    return
                }
                newContact.imageName = timestamp
                try newContactImage?.write(to: url)
            }
            try ContactDBHelper.insert(item: newContact)
            self.delegate?.didSaveContact()
        } catch {
            self.delegate?.didFailWith(error: error)
        }
    }

    func validateField(field: String, text: String?) {
        guard let text = text else {
            delegate?.enableCreation(false)
            return
        }

        switch field {
        case Field.firstName.rawValue:
            newContact.firstName = text
        case Field.lastName.rawValue:
            newContact.lastName = text
        case Field.phone.rawValue:
            newContact.phone = text
        default:
            newContact.email = text
            guard text.isValidEmail() else {
                delegate?.enableCreation(false)
                return
            }
        }
        delegate?.enableCreation(newContact.isAllInformationSet())
    }
}
