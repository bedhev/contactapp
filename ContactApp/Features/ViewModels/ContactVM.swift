//
//  ContactVM.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 03/06/2021.
//

import UIKit

protocol ContactVMDelegate: class {
    func didGetContactList()
    func didFailWith(error: Error)
}

final class ContactVM {

    private weak var delegate: ContactVMDelegate?
    private let semaphore = DispatchSemaphore(value: 0)
    private var contacts = [Contact]()
    private var sectionDictionary: [String: [Contact]] = [:]
    private var keysSorted: [String] {
        sectionDictionary.keys.sorted(by: < )
    }

    init(delegate: ContactVMDelegate?) {
        self.delegate = delegate
    }

    func getContactList() {
        if let contacts = try? ContactDBHelper.findAll(), contacts.count > 0 {
            self.contacts = contacts
            sectionDictionary = Dictionary(grouping: contacts, by: {
                //https://stackoverflow.com/a/64772432
                let name = $0.lastName
                let normalizedName = name.folding(options: [.diacriticInsensitive, .caseInsensitive], locale: .current)
                let firstCharAsString = String(normalizedName.first!).uppercased()
                return firstCharAsString
            })
            self.delegate?.didGetContactList()
        } else {
            fetchContacts()
        }
    }

    private func fetchContacts() {
        var result: ([Contact]?, Error?)
        DispatchQueue.global(qos: .default).async {
            self.fetchContacts { (data, error)  in
                result = (data, error)
            }
        }
        semaphore.wait()
        DispatchQueue.main.async {
            self.insertData(result: result)
        }
    }

    private func fetchContacts(completion: @escaping ([Contact]?, Error?) -> Void) {
        ContactNetworkHelper.shared.getContacts { [weak self] (contacts, error) in
            completion(contacts, error)
            self?.semaphore.signal()
        }
    }

    private func insertData(result: ([Contact]?, Error?)) {
        guard let _ = result.0 else {
            self.delegate?.didFailWith(error: result.1!)
            return
        }

        do {
            try ContactDBHelper.inserts(items: result.0)
            self.getContactList()
        } catch {
            self.delegate?.didFailWith(error: error)
        }
    }

    // MARK: - TableView
    func numberOfSections() -> Int {
        return sectionDictionary.count
    }

    func numberOfRowsInSection(_ section: Int) -> Int {
        return sectionDictionary[keysSorted[section]]?.count ?? 0
    }

    func titleForHeaderInSection(_ section: Int) -> String {
        return keysSorted[section]
    }

    func sectionIndexTitles() -> [String] {
        return keysSorted
    }

    func configCell(_ cell: ContactCell, index: IndexPath) {
        cell.selectionStyle = .none
        let key = keysSorted[index.section]
        guard let contact = sectionDictionary[key]?[index.row] else {
            return
        }

        cell.contactLabel.text = contact.fullName
        cell.contactFavorite.isHidden = !contact.isFavorite
        //TODO: cache the image to avoid to init them everytime the user scrolls
        guard let url = contact.imageURL else {
            cell.contactImage.image = UIImage(named: Images.defaultContact.rawValue)
            return
        }
        cell.contactImage.image = UIImage(contentsOfFile: url.path)
    }

    func didPressCreateContact() {
        guard let vc = delegate as? ContactListController else {
            return
        }

        let detail = ContactDetailViewController()
        vc.navigationController?.pushViewController(detail, animated: true)
    }
}
