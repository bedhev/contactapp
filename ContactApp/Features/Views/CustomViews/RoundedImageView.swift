//
//  RoundedImage.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 08/06/2021.
//

import UIKit

class RoundedImageView: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = self.frame.width/2.0
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        contentMode = .scaleToFill
    }
}
