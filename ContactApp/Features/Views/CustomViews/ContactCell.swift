//
//  ContactCell.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 03/06/2021.
//

import UIKit

class ContactCell: UITableViewCell {

    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactFavorite: UIImageView!
    @IBOutlet weak var contactLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
