//
//  ContactDetailViewController.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 08/06/2021.
//

import UIKit

class ContactDetailViewController: UIViewController {

    @IBOutlet private weak var contactImage: RoundedImageView!
    @IBOutlet private weak var firstNameField: UITextField!
    @IBOutlet private weak var lastNameField: UITextField!
    @IBOutlet private weak var emailField: UITextField!
    @IBOutlet private weak var phoneField: UITextField!
    @IBOutlet private weak var saveContactButton: UIButton!
    @IBOutlet private weak var activityLoader: UIActivityIndicatorView!

    private var imagePickerVC = UIImagePickerController()

    private lazy var menu: UIAlertController = {
        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        menu.addAction(UIAlertAction(title: "Gallery", style: .default) { [weak self] (_) in
            self?.didPressGallery()
        })
        menu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        return menu
    }()

    private var viewModel: ContactDetailVM!

    // MARK: - Life Cycle
    init(contact: Contact? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = ContactDetailVM(delegate: self, contact: contact)
        self.imagePickerVC.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - IBActions
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    @IBAction func didPressAddPicture(_ sender: Any) {
        present(menu, animated: true)
    }

    @IBAction func didPressSaveContact(_ sender: UIButton) {
        sender.isEnabled = false
        activityLoader.startAnimating()
        viewModel.didPressSaveContact()
    }

    func didPressGallery() {
        imagePickerVC.sourceType = .photoLibrary
        present(imagePickerVC, animated: true)
    }
}

// MARK: - ContactDetailVMDelegate
extension ContactDetailViewController: ContactDetailVMDelegate {
    func didFailWith(error: Error) {
        activityLoader.stopAnimating()
        saveContactButton.isEnabled = true
        presentOkAlert(title: "Error", message: error.localizedDescription)
    }

    func enableCreation(_ enabled: Bool) {
        saveContactButton.isEnabled = enabled
    }

    func didSaveContact() {
        activityLoader.stopAnimating()
        presentOkAlert(title: "Success", message: "Contact Saved")
    }
}

// MARK: - UITextFieldDelegate
extension ContactDetailViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.viewModel.validateField(field: textField.accessibilityIdentifier!, text: textField.text)
        }
        return true
    }
}
// MARK: - UIImagePickerControllerDelegate
extension ContactDetailViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            return
        }
        contactImage.image = image
        viewModel.didGetImage(image: image)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ContactDetailViewController: UINavigationControllerDelegate {
}
