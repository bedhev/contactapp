//
//  ViewController.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import UIKit

class ContactListController: UIViewController {
    // MARK: UI
    @IBOutlet private weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!

    // MARK: properties
    private var viewModel: ContactVM!

    // MARK: - Life Cycle
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        viewModel = ContactVM(delegate: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        activityLoader.startAnimating()
        configUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getContactList()
    }

    // MARK: - UI
    private func configUI() {
        configTableView()
    }

    private func configTableView() {
        self.tableView.register(UINib(nibName: Xibs.contactCell.rawValue, bundle: nil), forCellReuseIdentifier: Cells.contact.rawValue)
        self.tableView.tableFooterView = UIView()
    }

    // MARK: - IBActions
    @IBAction func didPressAddContact(_ sender: UIBarButtonItem) {
        viewModel.didPressCreateContact()
    }
}

// MARK: - TableView
extension ContactListController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Rows.contact.rawValue
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeaderInSection(section)

    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return viewModel.sectionIndexTitles()
    }
}

extension ContactListController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cells.contact.rawValue, for: indexPath) as? ContactCell else {
            return UITableViewCell()
        }
        viewModel.configCell(cell, index: indexPath)
        return cell
    }
}

// MARK: - ContactVMDelegate
extension ContactListController: ContactVMDelegate {
    func didGetContactList() {
        activityLoader.stopAnimating()
        tableView.isHidden = false
        tableView.reloadData()
    }

    func didFailWith(error: Error) {
        activityLoader.stopAnimating()
        presentOkAlert(title: "Error", message: error.localizedDescription)
    }
}
