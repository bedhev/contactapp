//
//  ContactDBHelper.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation
import SQLite

final class ContactDBHelper: DBHelper {

    private static let table = Table(Database.Contacts.table.rawValue)
    private static let id = Expression<Int>(Database.Contacts.id.rawValue)
    private static let firstName = Expression<String>(Database.Contacts.firstName.rawValue)
    private static let lastName = Expression<String>(Database.Contacts.lastName.rawValue)
    private static let email = Expression<String>(Database.Contacts.email.rawValue)
    private static let phone = Expression<String>(Database.Contacts.phone.rawValue)
    private static let imageName = Expression<String?>(Database.Contacts.imageName.rawValue)
    private static let isFavorite = Expression<Int>(Database.Contacts.isFavorite.rawValue)

    typealias T = Contact

    // MARK: - Insert
    @discardableResult static func insert(item: T) throws -> Int64 {
        guard let DB = SQLiteDatabaseConnection.shared.BBDB else {
            throw DatabaseAccessError.databaseConnectionError
        }
        let insert = table.insert(firstName <- item.firstName,
                                  lastName <- item.lastName,
                                  phone <- item.phone,
                                  email <- item.email,
                                  isFavorite <- item.isFavorite.intValue,
                                  imageName <- item.imageName
        )
        do {
            let rowId = try DB.run(insert)
            guard rowId > 0 else {
                throw DatabaseAccessError.insertError
            }
            return rowId
        } catch {
            Logger.error(error.localizedDescription)
            throw DatabaseAccessError.insertError
        }
    }

    static func inserts(items: [T]?) throws {
        guard let _ = items, items!.count > 0 else {
            throw DatabaseAccessError.countItemError
        }
        for item in items! {
            try self.insert(item: item)
        }
    }

    // MARK: - Delete
    static func delete (item: T) throws {
        guard let DB = SQLiteDatabaseConnection.shared.BBDB else {
            throw DatabaseAccessError.databaseConnectionError
        }
        let query = table.filter(id == item.id)
        do {
            try DB.run(query.delete())
        } catch {
            Logger.error(error.localizedDescription)
            throw DatabaseAccessError.deleteError(id: item.id)
        }
    }

    // MARK: - Find
    static func findAll() throws -> [T] {
        guard let DB = SQLiteDatabaseConnection.shared.BBDB else {
            throw DatabaseAccessError.databaseConnectionError
        }
        var retArray = [T]()

        let items = try DB.prepare(table)
        for item in items {
            retArray.append(Contact(id: item[id],
                                    firstName: item[firstName],
                                    lastName: item[lastName],
                                    email: item[email],
                                    phone: item[phone],
                                    isFavorite: item[isFavorite].boolValue,
                                    imageName: item[imageName])
            )
        }
        return retArray
    }

    // MARK: - Update
    static func update(oldItem: T, newItem: T) throws {
        guard let DB = SQLiteDatabaseConnection.shared.BBDB else {
            throw DatabaseAccessError.databaseConnectionError
        }
        let query = table.filter(id == oldItem.id)
        do {
            try DB.run(query.update(
                        firstName <- firstName.replace(oldItem.firstName, with: newItem.firstName),
                        lastName <- lastName.replace(oldItem.lastName, with: newItem.lastName),
                        phone <- phone.replace(oldItem.phone, with: newItem.phone),
                        email <- email.replace(oldItem.email, with: newItem.email))
            )
            //Chain query doesn't let update nil or int
            try DB.run(query.update(imageName <- newItem.imageName))
            try DB.run(query.update(isFavorite <- newItem.isFavorite.intValue))
        } catch {
            Logger.error(error.localizedDescription)
        }
    }
}
