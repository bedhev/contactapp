//
//  SQLiteDatabaseConnection.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation
import SQLite

final class SQLiteDatabaseConnection {
    static let shared = SQLiteDatabaseConnection()

    var BBDB: Connection? {
        guard let URL = FileSystem.documentsDirectory?.appendingPathComponent(Database.completeName) else {
            Logger.error(FilesError.documentDirectoryUnavailable.message)
            return nil
        }
        guard let DB = try? Connection(URL.path) else {
            Logger.error(DatabaseAccessError.databaseConnectionError.message)
            return nil
        }
        return DB
    }

    private init() {
    }
}
