//
//  DataAccessError.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 02/06/2021.
//

import Foundation

enum DatabaseAccessError: Error {
    case databaseConnectionError
    case insertError
    case deleteError(id: Int)
    case updateError(id: Int)
    case searchError
    case createItemError
    case nilInData
    case countItemError
}

// MARK: - Message
extension DatabaseAccessError {

    /// The reason for why the error occured.
    public var message: String {
        switch self {
        case .databaseConnectionError:
            return "Cannot connect to the database"
        case .insertError:
            return "Cannot insert the item"
        case .deleteError(let id):
            return "Cannot delete the item \(id)"
        case .updateError(let id):
            return "Cannot delete the item \(id)"
        default:
            return "Database operation error"
        }
    }
}
