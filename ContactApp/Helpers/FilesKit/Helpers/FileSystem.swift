//
//  FileSystem.swift
//  Adneom
//
//  Created by Bryan D'HAESELEER on 10/01/2020.
//  Copyright © 2020 Bryan D'HAESELEER. All rights reserved.
//

import Foundation

public final class FileSystem {
    public static let documentsDirectory: URL? = {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        return url
    }()

    public static let cacheDirectory: URL? = {
        let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        return url
    }()
}
