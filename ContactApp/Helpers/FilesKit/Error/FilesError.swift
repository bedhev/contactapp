//
//  FilesError.swift
//  Adneom
//
//  Created by Bryan D'HAESELEER on 10/01/2020.
//  Copyright © 2020 Bryan D'HAESELEER. All rights reserved.
//

import Foundation

public enum FilesError: Error {

    /// Cannot access to Document directory
    case documentDirectoryUnavailable
    /// Cannot access to Document directory
    case invalidsPath(_ path: String)
    /// A directory could not be created.
    case createDirectoryFailed(_ URL: String, error: Error)
    /// A file already exists at operation destination.
    case directoryAlreadyExists(_ URL: String)
    /// Cannot delete file or directory at operation destination.
    case delete(_ URL: String)
    /// Cannont copy source item at directory
    case copyFailed(_ error: Error)
    /// Cannont copy source item at directory
    case moveFailed(_ error: Error)
    /// Cannot convert to base64
    case base64FailEncode(_ name: String)
    /// Cannot write b64file
    case base64Failed(_ name: String, _ error: Error)
    /// Cannot write file
    case writeFailed(_ name: String, _ error: Error)
    /// Content isn't gettable
    case getContentFailed(_ URL: String, error: Error)
}

// MARK: - Message
extension FilesError {

    /// The reason for why the error occured.
    public var message: String {
        switch self {
        case .documentDirectoryUnavailable:
            return "Could not access to document directory"
        case .directoryAlreadyExists(let URL):
            return "Directory already exists at \(URL)"
        case .createDirectoryFailed(let URL, let error):
            return "Could not create a directory at \(URL) - \(error.localizedDescription)"
        case .delete(let URL):
            return "Could not delete at \(URL)"
        case .copyFailed(let error):
            return "Could not copy \(error.localizedDescription)"
        case .moveFailed(let error):
            return "Could not copy \(error.localizedDescription)"
        case .base64FailEncode(let name):
            return "Cannot convert \(name) to base64"
        case .base64Failed(let name, let error):
            return "Cannot write b64 \(name) - \(error.localizedDescription)"
        case .writeFailed(let name, let error):
            return "Cannot write \(name) - \(error.localizedDescription)"
        case .invalidsPath(let path):
            return "The given path \(path) isn't valid"
        case .getContentFailed(let URL, let error):
            return "Could not get content at \(URL) - \(error.localizedDescription)"
        }
    }
}
