//
//  FilesKit.swift
//  Adneom
//
//  Created by Bryan D'HAESELEER on 10/01/2020.
//  Copyright © 2020 Bryan D'HAESELEER. All rights reserved.
//

import Foundation

public final class FilesKit {
    public static let shared = FilesKit()

    /// Shared json decoder instance
    private static var jsonDecoder = JSONDecoder()
    /// Shared json encoder instance
    private static var jsonEncoder = JSONEncoder()
    /// FileManager.default is used because not delegate is involved
    private let manager = FileManager.default

    // MARK: private init
    private init() {
    }

    // MARK: - TRANSFER

    /**
     Make a copy of the file or directory at the specified URL to a new location.
     If the item at source is a directory, this method copies the directory and all of its contents, including any hidden files. If a file with the same name already exists at destination, this method stops the copy attempt and returns an appropriate error. If the last component of srcURL is a symbolic link, only the link is copied to the new path.
     
     - parameter at: The file URL that identifies the file you want to copy. The URL in this parameter must not be a file reference URL. This parameter must not be nil.
     - parameter to: The URL at which to place the copy of srcURL. The URL in this parameter must not be a file reference URL and must include the name of the file in its new location. This parameter must not be nil.
     */
    public func makeCopy(from source: URL, to destination: URL) throws {
        do {
            try manager.copyItem(at: source, to: destination)
        } catch {
            Logger.error("FilesError.copyFailed \(error.localizedDescription)")
            throw FilesError.copyFailed(error)
        }
    }

    /**
     Moves the file or directory at the specified URL to a new location synchronously.
     If the item at source is a directory, this method moves the directory and all of its contents, including any hidden files. If an item with the same name already exists at destination, this method stops the move attempt and returns an appropriate error
     
     - parameter at: The file URL that identifies the file you want to copy. The URL in this parameter must not be a file reference URL. This parameter must not be nil.
     - parameter to: The URL at which to place the copy of srcURL. The URL in this parameter must not be a file reference URL and must include the name of the file in its new location. This parameter must not be nil.
     */
    public func moveItem(from source: URL, to destination: URL) throws {
        do {
            try manager.moveItem(atPath: source.path, toPath: destination.path)
        } catch {
            Logger.error("FilesError.moveFailed \(error.localizedDescription)")
            throw FilesError.moveFailed(error)
        }
    }

    /**
     Copy any type of file present (alias resource) in the bundle into the document directory
     
     - parameter resource: The resource as String without its extension that identifies the file to copy.
     - parameter extension: The extension as String of the resource
     - parameter subPath: a specific path as String to add to the directory path, by default nil
     */
    @discardableResult public func bundleToDirectory(file resource: String, extension ext: String, subPath: String? = nil) throws -> URL? {
        guard let documentDirectory = FileSystem.documentsDirectory else {
            throw FilesError.documentDirectoryUnavailable
        }
        do {
            guard let bundlePath = Bundle.main.path(forResource: resource, ofType: ext) else {
                Logger.error("Error copyBundleToDirectory FilesError.invalidsPath(")
                throw FilesError.invalidsPath("\(resource).\(ext) not found")
            }
            var newFilePath: String!
            if subPath == nil {
                newFilePath = documentDirectory.appendingPathComponent("\(resource).\(ext)").path
            } else {
                newFilePath = documentDirectory.appendingPathComponent("\(subPath!)\(resource).\(ext)").path
            }
            try manager.copyItem(atPath: bundlePath, toPath: newFilePath)
            Logger.success("\(resource) copied in directory")
            return URL(string: newFilePath)
        } catch {
            Logger.error("Error copyBundleToDirectory: \(error.localizedDescription)")
            throw FilesError.copyFailed(error)
        }
    }

    // MARK: - DELETION
    /**
     Delete a file or a directory via its PATH
     - parameter path: The path of the file or directory
     */
    public func delete(at path: String) throws {
        do {
            guard let url = URL(string: path) else {
                Logger.error("copyBundleToDirectory FilesError.invalidsPath(")
                throw FilesError.invalidsPath(path)
            }
            try manager.removeItem(at: url)
        } catch {
            Logger.error(path+" || "+error.localizedDescription)
            throw FilesError.delete(path)
        }
    }

    /**
     Delete a file or a directory via its URL
     - parameter url: The url of the file or directory
     */
    public func delete(at url: URL) throws {
        do {
            try manager.removeItem(at: url)
        } catch {
            Logger.error("FilesError.delete \(error.localizedDescription)")
            throw FilesError.delete(url.path)
        }
    }

    // MARK: - GET
    /**
     This method performs a shallow search of the directory and therefore does not traverse symbolic links or return the contents of any subdirectories.
     This method also does not return URLs for the current directory (“.”), parent directory (“..”), or resource forks (files that begin with “._”)
     
     - returns: An array that contains the URLs of the directory's content.
     */
    public func getContent(at path: URL) throws -> [URL] {
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles])
            return directoryContents
        } catch {
            throw FilesError.getContentFailed(path.path, error: error)
        }
    }

    /**
     This method get all of the content of the document Directory excepted hidden files and hard delete everything
     */
    func eraseContent() {
        do {
            guard let documentDirectory = FileSystem.documentsDirectory else {
                Logger.error(FilesError.documentDirectoryUnavailable.localizedDescription)
                return
            }
            let fileURLs = try manager.contentsOfDirectory(at: documentDirectory,
                                                           includingPropertiesForKeys: nil,
                                                           options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            for fileURL in fileURLs {
                try manager.removeItem(at: fileURL)
            }
        } catch {
            Logger.error(error.localizedDescription)
            return
        }
    }

    // MARK: - Create
    /**
     Creates a directory with given attributes at the specified path.
     - parameter directory: The name of the directory
     - parameter createIntermediates: If true, this method creates any nonexistent parent directories as part of creating the directory in path. If false, this method fails if any of the intermediate parent directories does not exist. This method also fails if any of the intermediate path elements corresponds to a file and not a directory.
     */
    @discardableResult public func createDirectory(directory: String, withIntermediateDirectories createIntermediates: Bool = true) throws -> URL {
        guard let documentDirectory = FileSystem.documentsDirectory else {
            throw FilesError.documentDirectoryUnavailable
        }

        let filePath = documentDirectory.appendingPathComponent(directory)

        do {
            try self.createDirectory(path: filePath.path, withIntermediateDirectories: createIntermediates)
        } catch {
            Logger.error("Error createDirectoryFailed \(error.localizedDescription)")
            throw FilesError.createDirectoryFailed(filePath.path, error: error)
        }
        return filePath
    }

    /**
     Creates a directory with given attributes at the specified path.
     - parameters:
     - directories: The name of the directories
     - createIntermediates: If true, this method creates any nonexistent parent directories as part of creating the directory in path. If false, this method fails if any of the intermediate parent directories does not exist. This method also fails if any of the intermediate path elements corresponds to a file and not a directory.
     */
    public func createDirectories(directories: String ..., withIntermediateDirectories createIntermediates: Bool = true) throws -> [URL] {
        guard let documentDirectory = FileSystem.documentsDirectory else {
            throw FilesError.documentDirectoryUnavailable
        }
        var urls = [URL]()
        for path in directories {
            let url = documentDirectory.appendingPathComponent(path)
            do {
                try self.createDirectory(path: url.path, withIntermediateDirectories: createIntermediates)
                urls.append(url)
            } catch {
                throw FilesError.createDirectoryFailed(path, error: error)
            }
        }
        return urls
    }

    /**
     Write a Object that conforms to Encodable Protocol
     If no sub path is provided, by default the file will be write in Document directory, if a subPath is provided the file will be write in Document/subPath/fileName
     
     - parameter base64String: The base64 content represented by a String
     - parameter fileName: The file name for the file
     - parameter subPath: The sub path of the file as optionnal, defaut is nil
     */
    func writeBase64(_ base64String: String, fileName: String, subPath: String? = nil) throws {
        guard let documentDirectory = FileSystem.documentsDirectory else {
            throw FilesError.documentDirectoryUnavailable
        }
        guard let convertedData = Data(base64Encoded: base64String) else {
            throw FilesError.base64FailEncode(fileName)
        }
        var completePath: URL!
        if let path = subPath {
            completePath = documentDirectory.appendingPathComponent(path)
            //try to create directory
            try self.createDirectory(path: completePath!.path, withIntermediateDirectories: true)
        } else {
            completePath = documentDirectory
        }
        do {
            try self.writeFile(convertedData, url: completePath)
        } catch {
            Logger.error(error.localizedDescription)
            throw FilesError.base64Failed(fileName, error)
        }
    }

    /**
     Write a Object that conforms to Encodable Protocol
     If no sub path is provided, by default the file will be write in Document directory, if a subPath is provided the file will be write in Document/subPath/fileName
     
     - parameter json: The Encodable object to write in the give path.
     - parameter fileName: The file name for the file
     - parameter subPath: The sub path of the file as optionnal, defaut is nil
     */
    func writeJSON<T: Encodable>(_ json: T, fileName: String, subPath: String? = nil) throws {
        guard let documentDirectory = FileSystem.documentsDirectory else {
            throw FilesError.documentDirectoryUnavailable
        }
        var completePath: URL!
        if let path = subPath {
            completePath = documentDirectory.appendingPathComponent(path)
            //try to create directory
            try self.createDirectory(path: completePath!.path, withIntermediateDirectories: true)
        } else {
            completePath = documentDirectory
        }

        //add file name to path
        completePath = completePath.appendingPathComponent(fileName)
        do {
            let data = try JSONEncoder().encode(json)
            try self.writeFile(data, url: completePath!)
        } catch {
            Logger.error(error.localizedDescription)
            throw FilesError.writeFailed(fileName, error)
        }
    }

    // MARK: - Convenients function
    /**
     Creates a directory with given attributes at the specified path.
     */
    private func createDirectory(path: String, withIntermediateDirectories createIntermediates: Bool) throws {
        //createDirectory return: true if the directory was created, true if createIntermediates is set and the directory already exists, or false if an error occurred.
        try manager.createDirectory(atPath: path,
                                    withIntermediateDirectories: createIntermediates,
                                    attributes: nil)
    }

    /**
     Write a file with the given data at the specified path.
     If the file exists it will be override by the new one.
     */
    private func writeFile(_ data: Data, url: URL) throws {
        manager.createFile(atPath: url.path, contents: data, attributes: nil)
    }
}
