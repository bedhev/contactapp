//
//  ContactNetworkHelper.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 3/06/2021.
//

import Foundation
import Moya

final class ContactNetworkHelper: BaseNetwork {

    static let shared = ContactNetworkHelper()
    private var provider = MoyaProvider<ContactEndpoints>(
        callbackQueue: DispatchQueue.global(qos: . default),
        plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])

    func getContacts(completion: @escaping ([Contact]?, Error?) -> Void) {
        provider.request(.getAllContacts) { [weak self] (result) in
            self?.processResponse(result: result, decodable: ContactWrapper.self,
                                  completion: { (contactWrapper, error) in
                completion(contactWrapper?.contacts, error)
            })
        }
    }

    func createContact(contact: Contact, completion: @escaping (Contact?, Error?) -> Void) {
        provider.request(.createContact(contact: contact)) { [weak self] (result) in
            self?.processResponse(result: result, decodable: ContactWrapper.self,
                                  completion: { (contactWrapper, error) in
                completion(contactWrapper?.contact, error)
            })
        }
    }
}
