//
//  ContactEndpoints.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 3/06/2021.
//

import Foundation
import Moya

enum ContactEndpoints {
    case getAllContacts
    case createContact(contact: Contact)
    case updateContact
}

extension ContactEndpoints: Endpoint {
    var baseURL: URL {
        return URL(string: Base.URL)!
    }

    var path: String {
        return "/contacts"
    }

    var method: Moya.Method {
        switch self {
        case .getAllContacts:
            return .get
        default:
            return .post
        }
    }

    var task: Task {
        switch self {
        case .getAllContacts:
            return .requestPlain
        case .createContact(let contact):
            let allowedParams = ["firstname": contact.firstName,
                                 "lastname": contact.lastName,
                                 "email": contact.email,
                                 "phone": contact.phone]
            return .requestParameters(parameters: allowedParams, encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
}
