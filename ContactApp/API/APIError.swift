//
//  APIError.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 3/06/2021.
//

import Foundation

enum APIError: Error {
    case unauthorized(path: String?)
    case notFound(path: String?)
    case forbidden(path: String?)
    case serverIssue
    case unknownIssue
}

extension APIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .unauthorized(let path):
            return "Unauthorized \(path ?? "unknown") operation"
        case .notFound(let path):
            return "Not found \(path ?? "unknown") operation"
        case .forbidden(let path):
            return "Forbidden operation at \(path ?? "unknown")"
        case .serverIssue:
            return "Server not reachable"
        case .unknownIssue:
            return "Oopsi"
        }
    }
}
