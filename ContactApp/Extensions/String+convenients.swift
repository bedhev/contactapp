//
//  String+Convenients.swift
//  Adneom
//
//  Created by Bryan D'HAESELEER on 14/01/2019.
//  Copyright © 2019 Bryan D'HAESELEER. All rights reserved.
//

import Foundation
/// String+convenients contains all the convenients tools we use in String
extension String {

    /// Check if the String contains any
    ///
    /// - returns: A Boolean value indicating whether the string is empty or not
    public func isEmptySpace() -> Bool {
        return (self.isEmpty || self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
    }

    /// Validate email string
    ///
    /// - returns: A Boolean value indicating whether an email is valid.
    func isValidEmail() -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}"
            + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
            + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
            + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
            + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
            + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
            + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    /// https://stackoverflow.com/a/61957694
    func isPasswordValid() -> Bool {
        guard !self.isEmpty else {
            return false
        }

        let passwordRegEx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{8,}$"
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", passwordRegEx)
        return emailTest.evaluate(with: self)
    }
}
