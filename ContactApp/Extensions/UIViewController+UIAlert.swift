//
//  UIViewController+alert.swift
//  ContactApp
//
//  Created by Bryan D'HAESELEER on 03/06/2021.
//

import UIKit

extension UIViewController {

    func presentOkAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
